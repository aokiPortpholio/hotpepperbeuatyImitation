package com.hotpepper.beauty.demo.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotpepper.beauty.demo.common.MyLogger;
import com.hotpepper.beauty.demo.model.MenuDetail;
import com.hotpepper.beauty.demo.model.enums.MenuItem;
import com.hotpepper.beauty.demo.model.form.MenuFilter;
import com.hotpepper.beauty.demo.repository.MenuRepository;
import com.hotpepper.beauty.demo.repository.dao.MenuDetailSelect;

@Service
public class MenuService {
	private MenuRepository repository;
	
	@Autowired
	public MenuService(MenuRepository repository) {
		this.repository = repository;
	}
	
	// 全件検索
	public List<MenuDetail> getMenuListAll() {
		List<MenuDetailSelect> selectDtl = repository.selectMenuDetailAll();
		return getMenuDetail(selectDtl);
	}
	
	// 絞り込み検索
	public List<MenuDetail> getMenuList(MenuFilter filter) {
		
		// チェックボックスからSQLのIN句を生成
		Set<Integer> param = new HashSet<Integer>();
		if (filter.getChkComplex() != null) {
			param.add(MenuItem.TREATMENT.getCode());
			param.add(MenuItem.HAIRSET.getCode());
			param.add(MenuItem.HEADSPA.getCode());
			param.add(MenuItem.OTHER.getCode());
		}
		
		for (String chked : filter.getChkboxs()) {
			for (MenuItem item : MenuItem.values()) {
				if (chked.equals(item.getKind())) {
					param.add(item.getCode());
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		param.forEach(s -> sb.append(s).append(","));
		if (sb.length() != 0) sb.deleteCharAt(sb.length()-1);
		
		MyLogger.logging("   SQLのIN句 ：" + sb.toString());

		List<MenuDetailSelect> selectDtl;
		if (sb.length() == 0) {
			selectDtl = repository.selectMenuDetailAll();
		} else {
			selectDtl = repository.selectMenuDetailList(sb.toString());
		}
		
		return getMenuDetail(selectDtl);
	}
	
	private List<MenuDetail> getMenuDetail(List<MenuDetailSelect> selectList) {
		
		List<MenuDetail> result = new ArrayList<MenuDetail>();
		for (MenuDetailSelect dtlSlct : selectList) {

			List<Integer> menuKinds = repository.selectMenuKind(dtlSlct.getId());
			MenuDetail dtl = MenuDetail.getMenuDetail(dtlSlct, menuKinds);
			
			result.add(dtl);
		}
		return result;
	}
}
