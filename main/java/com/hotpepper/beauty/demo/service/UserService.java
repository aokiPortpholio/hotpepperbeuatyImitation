package com.hotpepper.beauty.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotpepper.beauty.demo.repository.UserRepository;
import com.hotpepper.beauty.demo.repository.dao.Users;

@Service
public class UserService {
	private final UserRepository userRepository;
	
	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public Users getUsers(String loginId) {
		return userRepository.selectUsers(loginId);
	}
}
