package com.hotpepper.beauty.demo.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotpepper.beauty.demo.common.MyLogger;
import com.hotpepper.beauty.demo.model.ImmutableActiveStylists;
import com.hotpepper.beauty.demo.model.ImmutableBookTable;
import com.hotpepper.beauty.demo.model.MenuDetail;
import com.hotpepper.beauty.demo.model.enums.Timezone;
import com.hotpepper.beauty.demo.repository.BookRepository;
import com.hotpepper.beauty.demo.repository.MenuRepository;
import com.hotpepper.beauty.demo.repository.dao.Book;
import com.hotpepper.beauty.demo.repository.dao.DayOff;
import com.hotpepper.beauty.demo.repository.dao.MenuDetailSelect;
import com.hotpepper.beauty.demo.repository.dao.ShiftType;
import com.hotpepper.beauty.demo.repository.dao.Stylist;

@Service
public class BookService {

	private MenuRepository menuRepository;
	private BookRepository bookRepository;
	
	@Autowired
	public BookService(MenuRepository menuRepository, BookRepository bookRepository) {
		this.menuRepository = menuRepository;
		this.bookRepository = bookRepository;
	}
	
	public MenuDetail selectMenuDetail(String menuId) {
		MenuDetailSelect selectDtl = menuRepository.selectMenuDetail(menuId);
		MenuDetail menuDtl = MenuDetail.getMenuDetail(selectDtl, new ArrayList<Integer>());
		
		return menuDtl;
	}
	
	public Stylist selectStylist(String stylistId) {
		List<Stylist> stylistList = bookRepository.selectStylistList(stylistId);
		
		return stylistList.get(0);
	}
	
	public ImmutableBookTable getImmutableBookTableMap(String stylistId) {
		int date = 20210406;
		List<Stylist> stylistList = bookRepository.selectStylistList(stylistId);
		
		StringBuilder sb = new StringBuilder();
		stylistList.forEach(s -> sb.append(s.getShiftType()).append(","));
		sb.deleteCharAt(sb.length()-1);
		
		List<ShiftType> shiftTypeList = bookRepository.selectShiftType(sb.toString());
		shiftTypeList.forEach(MyLogger::logging);
		
		List<DayOff> dayOffList = bookRepository.selectDayOff(date, stylistId);
		dayOffList.forEach(MyLogger::logging);
		
		List<Book> bookList = bookRepository.selectBook(stylistId);
		bookList.forEach(MyLogger::logging);
		
		ImmutableBookTable bookTable = ImmutableBookTable.getInstance(date, stylistList, shiftTypeList, dayOffList, bookList);
		bookTable.getHead().forEach(MyLogger::logging);
		bookTable.getBody().entrySet().forEach(e -> MyLogger.logging(e.getKey() + e.getValue()));
		
		return bookTable;
	}
	
	public Map<String, List<Stylist>> getUnmodifiableStylistMap() {
		List<Stylist> stylistList = bookRepository.selectStylistList(null);
		stylistList.forEach(s -> s.setCutExamples(bookRepository.selectCutExamplePictures(s.getStylistId())));
		stylistList.stream().filter(s -> s.getCutExamples() == null).forEach(f -> f.setCutExamples(Collections.emptyList()));
		stylistList.forEach(MyLogger::logging);
		
		return ImmutableActiveStylists.getInstance(stylistList).getActiveStylistMap();
	}
	
	public String getVisitDate(String date) {
		int year = Integer.valueOf(String.valueOf(date).substring(0, 4));
		int month = Integer.valueOf(String.valueOf(date).substring(4, 6));
		int day = Integer.valueOf(String.valueOf(date).substring(6, 8));
		LocalDate lDate = LocalDate.of(year, month, day);
		
		return String.format("%s（%s）",
				lDate.format(DateTimeFormatter.ofPattern("MM月dd日")),
				lDate.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.JAPANESE));
	}
	
	public ImmutableActiveStylists getUnmodifiableActiveStylistMap(String date, String time) {
		String stylistId = null;
		Timezone tz = (Timezone)Arrays.stream(Timezone.values()).filter(t -> t.getDispValue().equals(time)).toArray()[0];
		
		List<Stylist> stylistList = bookRepository.selectStylistList(stylistId);
		stylistList.forEach(s -> s.setCutExamples(bookRepository.selectCutExamplePictures(s.getStylistId())));
		stylistList.stream().filter(s -> s.getCutExamples() == null).forEach(f -> f.setCutExamples(Collections.emptyList()));
		
		StringBuilder sb = new StringBuilder();
		stylistList.forEach(s -> sb.append(s.getShiftType()).append(","));
		sb.deleteCharAt(sb.length()-1);
		
		List<ShiftType> shiftTypeList = bookRepository.selectShiftType(sb.toString());
		shiftTypeList.forEach(MyLogger::logging);
		
		List<DayOff> dayOffList = bookRepository.selectDayOff(Integer.valueOf(date), stylistId);
		dayOffList.forEach(MyLogger::logging);
		
		List<Book> bookList = bookRepository.selectBook(stylistId);
		bookList.forEach(MyLogger::logging);
		
		return ImmutableActiveStylists.getInstance(date, tz, stylistList, shiftTypeList, dayOffList, bookList);
	}
}
