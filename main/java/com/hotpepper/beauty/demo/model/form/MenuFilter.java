package com.hotpepper.beauty.demo.model.form;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class MenuFilter {
	public static final String ON = "on";
	
	private String chkboxs[];
	private String chkComplex;
}
