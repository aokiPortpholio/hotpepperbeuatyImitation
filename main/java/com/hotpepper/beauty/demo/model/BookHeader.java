package com.hotpepper.beauty.demo.model;

import lombok.Data;

@Data
public class BookHeader {
	private String yearMonth;
	private String date;
	private String weekType;
}
