package com.hotpepper.beauty.demo.model;

import lombok.Data;

@Data
public class BookBody {
	private final String status;
	private final String date;
}
