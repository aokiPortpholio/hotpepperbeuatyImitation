package com.hotpepper.beauty.demo.model.enums;

public enum WeekKind {
	MONDAY("月", "monday", 1),
	TUESDAY("火", "tuesday", 2),
	WENDSDAY("水", "wendsday", 3),
	THURSDAY("木", "thursday", 4),
	FRIDAY("金", "friday", 5),
	SATURDAY("土", "saturday", 6),
	SUNDAY("日", "sunday", 7);
	
	public String dispValue;
	public String strValue;
	public int code;
	
	private WeekKind(String dispValue, String strValue, int code) {
		this.dispValue = dispValue;
		this.strValue = strValue;
		this.code = code;
	}
	
	public String getDispValue() {
		return dispValue;
	}
	public String getStrValue() {
		return strValue;
	}
	public int getCode() {
		return code;
	}
}
