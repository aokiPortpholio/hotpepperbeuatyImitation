package com.hotpepper.beauty.demo.model.enums;

public enum CouponScope {
	NEW("新規", "newcomer", 0),
	ALL("全員", "everybody", 1);
	
	private String dispValue;
	private String strValue;
	private int code;
	
	private CouponScope(String dispValue, String strValue, int code) {
		this.dispValue = dispValue;
		this.strValue = strValue;
		this.code = code;
	}
	
	public String getDispValue() {
		return dispValue;
	}
	public String getStrValue() {
		return strValue;
	}
	public int getCode() {
		return code;
	}
}
