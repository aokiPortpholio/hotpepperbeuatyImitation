package com.hotpepper.beauty.demo.model.enums;

public enum MenuItem {
	CUT("カット", "cut", 0),
	COLOR("カラー", "color", 1),
	PERM("パーマ", "perm", 2),
	STRAIGHT("縮毛矯正", "straight", 3),
	EXTENTION("エクステ", "extention", 4),
	TREATMENT("トリートメント", "treatment", 5),
	HAIRSET("ヘアセット", "hairset", 6),
	HEADSPA("ヘッドスパ", "headspa", 7),
	DRESSING("着付け", "dressing", 8),
	OTHER("その他", "ohter", 9);
	
	private String disp;
	private String kind;
	private int code;
	
	private MenuItem(String disp, String kind, int code) {
		this.disp = disp;
		this.kind = kind;
		this.code = code;
	}
	
	public String getDisp() {
		return disp;
	}
	public String getKind() {
		return kind;
	}
	public int getCode() {
		return code;
	}
}
