package com.hotpepper.beauty.demo.model.enums;

public enum Timezone {
	TIMEZONE1("09:30", "0930", 0),
	TIMEZONE2("10:00", "1000", 1),
	TIMEZONE3("10:30", "1030", 2),
	TIMEZONE4("11:00", "1100", 3),
	TIMEZONE5("11:30", "1130", 4),
	TIMEZONE6("12:00", "1200", 5),
	TIMEZONE7("12:30", "1230", 6),
	TIMEZONE8("13:00", "1300", 7),
	TIMEZONE9("13:30", "1330", 8),
	TIMEZONE10("14:00", "1400", 9),
	TIMEZONE11("14:30", "1430", 10),
	TIMEZONE12("15:00", "1500", 11),
	TIMEZONE13("15:30", "1530", 12),
	TIMEZONE14("16:00", "1600", 13),
	TIMEZONE15("16:30", "1630", 14),
	TIMEZONE16("17:00", "1700", 15),
	TIMEZONE17("17:30", "1730", 16),
	TIMEZONE18("18:00", "1800", 17),
	TIMEZONE19("18:30", "1830", 18),
	TIMEZONE20("19:00", "1900", 19),
	TIMEZONE21("19:30", "1930", 20),
	TIMEZONE22("20:00", "2000", 21),
	TIMEZONE23("20:30", "2030", 22),
	TIMEZONE24("21:00", "2100", 23);

	public String dispValue;
	public String strValue;
	public int code;
	
	private Timezone(String dispValue, String strValue, int code) {
		this.dispValue = dispValue;
		this.strValue = strValue;
		this.code = code;
	}
	
	public String getDispValue() {
		return dispValue;
	}
	public String getStrValue() {
		return strValue;
	}
	public int getCode() {
		return code;
	}
}
