package com.hotpepper.beauty.demo.model.enums;

public enum VisitCond {
	WEEKDAY("平日", "weekday", 0),
	HOLIDAY("休日", "holiday", 1),
	NOTHING("特になし", "nothing", 2);
	
	private String dispValue;
	private String strValue;
	private int code;
	
	private VisitCond(String dispValue, String strValue, int code) {
		this.dispValue = dispValue;
		this.strValue = strValue;
		this.code = code;
	}
	
	public String getDispValue() {
		return this.dispValue;
	}
	public String getStrValue() {
		return this.strValue;
	}
	public int getCode() {
		return this.code;
	}
}
