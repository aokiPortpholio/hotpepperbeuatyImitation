package com.hotpepper.beauty.demo.model.enums;

public enum UserRole {
	ROLE_USER("ROLE_USER", 0),
	ROLE_ADMIN("ROLE_ADMIN", 1);
	
	private String roleName;
	private int roleType;
	private UserRole(String roleName, int roleType) {
		this.roleName = roleName;
		this.roleType = roleType;
	}
	
	public String getRoleName() {
		return roleName;
	}
	public int getRoleType() {
		return roleType;
	}
}
