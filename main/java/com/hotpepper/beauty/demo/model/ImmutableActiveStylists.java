package com.hotpepper.beauty.demo.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hotpepper.beauty.demo.model.enums.Timezone;
import com.hotpepper.beauty.demo.repository.dao.Book;
import com.hotpepper.beauty.demo.repository.dao.DayOff;
import com.hotpepper.beauty.demo.repository.dao.ShiftType;
import com.hotpepper.beauty.demo.repository.dao.Stylist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImmutableActiveStylists {

	private final Map<String, List<Stylist>> activeStylistMap;
	private final Map<String, List<Stylist>> nonActiveStylistMap;

	private static final int STYLIST_LIST_DISP_LENGTH = 5;
	
	// コンストラクタはプライベート
	private ImmutableActiveStylists(Map<String, List<Stylist>> activeStylistMap, Map<String, List<Stylist>> nonActiveStylistMap) {
		this.activeStylistMap = activeStylistMap;
		this.nonActiveStylistMap = nonActiveStylistMap;
	}

	// 実質的なコンストラクタ（全員アクティブ（日時指定なし））
	public static ImmutableActiveStylists getInstance(List<Stylist> stylistList) {

		Map<String, List<Stylist>> stylistMap = getDispMap(stylistList);
		
		// 不変クラス
		return new ImmutableActiveStylists(Collections.unmodifiableMap(stylistMap), Collections.emptyMap());
	}

	// 実質的なコンストラクタ（アクティブ／非アクティブ分別）
	public static ImmutableActiveStylists getInstance(String date, Timezone tz, List<Stylist> stylistList,
			List<ShiftType> shiftTypeList, List<DayOff> dayOffList, List<Book> bookList) {
		
		int year = Integer.valueOf(String.valueOf(date).substring(0, 4));
		int month = Integer.valueOf(String.valueOf(date).substring(4, 6));
		int day = Integer.valueOf(String.valueOf(date).substring(6, 8));
		LocalDate lDate = LocalDate.of(year, month, day);
		
		Set<Stylist> activeSylists = new LinkedHashSet<>();
		Set<Stylist> nonActiveStylists = new LinkedHashSet<>();
		
		for (Stylist styl : stylistList) {
			boolean activeflg = false; 
			for (ShiftType shift : shiftTypeList) {
				if (isWorkin(tz, lDate, styl, shift)) {
					activeflg = true;
					break;
				}
			}
			if (!activeflg) {
				nonActiveStylists.add(styl);
				continue;
			}
			
			if (isDayOff(tz, lDate, dayOffList)) {
				nonActiveStylists.add(styl);
				continue;
			}
			if (isBooked(tz, lDate, bookList)) {
				nonActiveStylists.add(styl);
				continue;
			}
			activeSylists.add(styl);
		}
		
		// 不変クラス
		return new ImmutableActiveStylists(
				Collections.unmodifiableMap(getDispMap(new ArrayList<>(activeSylists))),
				Collections.unmodifiableMap(getDispMap(new ArrayList<>(nonActiveStylists))));
	}

	// 画面表示がスタイリスト5人で1行なので、5人ずつのリストに分割し、マップに収納する
	private static Map<String, List<Stylist>> getDispMap(List<Stylist> stylistList) {
		Map<String, List<Stylist>> stylistMap = new LinkedHashMap<>();
		List<Stylist> slist = null;
		for (int i = 0; i < stylistList.size(); i++) {
			if (i % STYLIST_LIST_DISP_LENGTH == 0) {
				if (slist != null) stylistMap.put(String.valueOf(i/STYLIST_LIST_DISP_LENGTH), slist);
				slist = new ArrayList<>();
			}
			slist.add(stylistList.get(i));
		}
		if (slist != null && slist.size() != 0)
		stylistMap.put(String.valueOf(stylistList.size()/STYLIST_LIST_DISP_LENGTH +1), slist);
		
		return stylistMap;
	}

	private static boolean isWorkin(Timezone tz, LocalDate lDate, Stylist styl, ShiftType shift) {
		if (styl.getShiftType() == shift.getShiftType() && lDate.getDayOfWeek().getValue() == shift.getWeekKind()
				&& tz.getCode() >= shift.getWorkInTimezone() && tz.getCode() <= shift.getWorkOutTimezone()) {

			return true;
		}
		return false;
	}
	
	private static boolean isDayOff(Timezone tz, LocalDate ldate, List<DayOff> dayOffList) {
		return dayOffList.stream().anyMatch(
				df -> String.valueOf(df.getDate()).equals(ldate.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
						&& df.getTimezone() == tz.getCode());
	}
	
	private static boolean isBooked(Timezone tz, LocalDate ldate, List<Book> bookList) {
		return bookList.stream().anyMatch(
				bk -> String.valueOf(bk.getDate()).equals(ldate.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
						&& bk.getTimezone() == tz.getCode());
	}
}
