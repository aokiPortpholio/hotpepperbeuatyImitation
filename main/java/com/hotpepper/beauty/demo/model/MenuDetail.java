package com.hotpepper.beauty.demo.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.hotpepper.beauty.demo.model.enums.CouponScope;
import com.hotpepper.beauty.demo.model.enums.MenuItem;
import com.hotpepper.beauty.demo.model.enums.VisitCond;
import com.hotpepper.beauty.demo.repository.dao.MenuDetailSelect;

import lombok.Data;

@Data
public class MenuDetail {

	@NotBlank
	@Pattern(regexp = "[a-zA-Z0-9]*")
	private String id;
	
	private List<String> menuKind;
	
	@NotBlank
	private String name;
	
	private String explanation;
	
	@Min(0)
	private int price;
	
	@Min(0)
	private int requiredTime;
	
	private String couponScope;
	
	private String visitCondition;
	
	private String otherCondition;
	
	@NotNull
	private String picture;
	
	public static MenuDetail getMenuDetail(MenuDetailSelect menuSlct, List<Integer> menuKinds) {
		MenuDetail dtl = new MenuDetail();
		
		// メニューID
		dtl.setId(menuSlct.getId());
		// メニュー種別取得
		List<String> menuList = new ArrayList<>();
		for (int mk : menuKinds) {
			Arrays.asList(MenuItem.values()).stream().filter(v -> v.getCode() == mk).forEach(v -> menuList.add(v.getDisp()));
		}
		dtl.setMenuKind(menuList);
		// 名前
		dtl.setName(menuSlct.getName());
		// 説明
		dtl.setExplanation(menuSlct.getExplanation());
		// 価格
		dtl.setPrice(menuSlct.getPrice());
		// 所要時間
		dtl.setRequiredTime(menuSlct.getRequiredTime());
		//　クーポンスコープ
		for (CouponScope cs : CouponScope.values()) {
			if (menuSlct.getCouponScope() == cs.getCode()) dtl.setCouponScope(cs.getDispValue());
		}
		//　来店条件
		for (VisitCond vc : VisitCond.values()) {
			if (menuSlct.getVisitCondition() == vc.getCode()) dtl.setVisitCondition(vc.getDispValue());
		}
		//　その他条件
		dtl.setOtherCondition(menuSlct.getOtherCondition());
		// 写真
		dtl.setPicture(menuSlct.getPictures());
		
		return dtl;
	}
}
