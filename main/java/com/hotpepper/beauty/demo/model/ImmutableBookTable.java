package com.hotpepper.beauty.demo.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.hotpepper.beauty.demo.model.enums.Timezone;
import com.hotpepper.beauty.demo.repository.dao.Book;
import com.hotpepper.beauty.demo.repository.dao.DayOff;
import com.hotpepper.beauty.demo.repository.dao.ShiftType;
import com.hotpepper.beauty.demo.repository.dao.Stylist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImmutableBookTable {

	private final List<BookHeader> head;
	private final Map<String, List<BookBody>> body;
	private final Map<String, Integer> headYM;

	private static final int DAYS_IN_TWO_WEEK = 14;

	// コンストラクタはプライベート
	private ImmutableBookTable(List<BookHeader> head, Map<String, List<BookBody>> body, Map<String, Integer> headYM) {
		this.head = head;
		this.body = body;
		this.headYM = headYM;
	}

	// 実質的なコンストラクタ
	public static ImmutableBookTable getInstance(int date, List<Stylist> stylistList, List<ShiftType> shiftTypeList,
			List<DayOff> dayOffList, List<Book> bookList) {

		// 不変クラス
		List<BookHeader> tmpHead = Collections.unmodifiableList(getHeader(date));
		Map<String, List<BookBody>> tmpBody = Collections.unmodifiableMap(getMap(date, stylistList, shiftTypeList, dayOffList, bookList));
		Map<String, Integer> tmpHeadYM = Collections.unmodifiableMap(getHeadYM(tmpHead));
		
		return new ImmutableBookTable(tmpHead, tmpBody, tmpHeadYM);
	}

	private static List<BookHeader> getHeader(int date) {

		int year = Integer.valueOf(String.valueOf(date).substring(0, 4));
		int month = Integer.valueOf(String.valueOf(date).substring(4, 6));
		int day = Integer.valueOf(String.valueOf(date).substring(6, 8));
		LocalDate startDate = LocalDate.of(year, month, day);

		List<BookHeader> headerList = new ArrayList<>();
		for (long i = 0; i < DAYS_IN_TWO_WEEK; i++) {
			LocalDate addedDate = startDate.plusDays(i);
			BookHeader header = new BookHeader();
			header.setYearMonth(addedDate.format(DateTimeFormatter.ofPattern("yyyy年M月")));
			header.setDate(addedDate.format(DateTimeFormatter.ofPattern("d")));
			header.setWeekType(addedDate.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.JAPANESE));

			headerList.add(header);
		}
		return headerList;
	}

	private static Map<String, List<BookBody>> getMap(int date, List<Stylist> stylistList, List<ShiftType> shiftTypeList,
			List<DayOff> dayOffList, List<Book> bookList) {
		
		LocalDate lDate = LocalDate.parse(String.valueOf(date), DateTimeFormatter.ofPattern("yyyyMMdd"));

		// 予約時間帯（画面のテーブルの横のラインごとに14日分の予約状況のリストを作成
		Map<String, List<BookBody>> bookTableMap = new LinkedHashMap<>();
		for (Timezone tz : Timezone.values()) {
			List<BookBody> bookStatusList = new ArrayList<>();
			for (int i = 0; i < DAYS_IN_TWO_WEEK; i++) {
				LocalDate plusDate = lDate.plusDays(i);
				
				// シフトタイプから予約時間帯ごとの勤務者数を算出
				int workInCnt = getWorkIncnt(tz.getCode(), plusDate, stylistList, shiftTypeList);
				
				// 休暇を引く
				workInCnt -= excludeDayOff(tz.getCode(), plusDate, dayOffList);
				
				// 予約と比較して半分切ってたら予約状況を△にする。埋まってたら×にする。
				String bookStatus = getBookStatus(tz.getCode(), plusDate, bookList, workInCnt);
				
				bookStatusList.add(new BookBody(bookStatus, plusDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")).toString()));
			}
			bookTableMap.put(tz.getDispValue(), bookStatusList);
		}

		return bookTableMap;
	}

	private static int getWorkIncnt(Integer timeZone, LocalDate lDate, List<Stylist> stylistList, List<ShiftType> shiftTypeList) {
		int cnt = 0;
		for (Stylist styl : stylistList) {
			for (ShiftType shift : shiftTypeList) {
				if (isWorkin(timeZone, lDate, styl, shift)) {
					cnt++;
				}
			}
		}
		return cnt;
	}

	private static boolean isWorkin(Integer timeZone, LocalDate lDate, Stylist styl, ShiftType shift) {
		if (styl.getShiftType() == shift.getShiftType() && lDate.getDayOfWeek().getValue() == shift.getWeekKind()
				&& timeZone >= shift.getWorkInTimezone() && timeZone <= shift.getWorkOutTimezone()) {

			return true;
		}
		return false;
	}
	
	private static int excludeDayOff(int tz, LocalDate ldate, List<DayOff> dayOffList) {
		int excludeCnt = (int)dayOffList.stream()
				.filter(dOff -> String.valueOf(dOff.getDate())
						.equals(ldate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))) && dOff.getTimezone() == tz)
				.count();
		
		return excludeCnt;
	}
	
	private static String getBookStatus(int tz, LocalDate ldate, List<Book> bookList, int workInCnt) {
		int bookCnt = (int)bookList.stream()
				.filter(dOff -> String.valueOf(dOff.getDate())
						.equals(ldate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))) && dOff.getTimezone() == tz)
				.count();
		
		if (workInCnt <= 0) {
			return "×";
		}
		if (workInCnt - bookCnt > workInCnt / 2.0) {
			return "◎";
		} else {
			return "△";
		}
	}
	
	private static Map<String, Integer> getHeadYM(List<BookHeader> hList) {
		List<String> monthList = hList.stream().map(h -> h.getYearMonth()).distinct().sorted().collect(Collectors.toList());
		long nowMonthCnt = hList.stream().filter(h -> h.getYearMonth().equals(monthList.get(0))).count();
		
		Map<String, Integer> map = new LinkedHashMap<>();
		map.put(monthList.get(0), (int)nowMonthCnt);
		if (monthList.size() > 1) {
			map.put(monthList.get(1), DAYS_IN_TWO_WEEK - (int)nowMonthCnt);
		}
		
		return map;
	}
}
