package com.hotpepper.beauty.demo.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.hotpepper.beauty.demo.model.enums.UserRole;
import com.hotpepper.beauty.demo.repository.dao.Users;

import lombok.Data;

@Data
public class HotPepperBeautyUser {
	private final String userId;
	private final String pswd;
	private final String role;
	
	private HotPepperBeautyUser(Users users, String role) {
		this.userId = users.getUserId();
		this.pswd = users.getPswd();
		this.role = role;
	}
	
	public static HotPepperBeautyUser getInstance(Users users) {
		List<UserRole> ur = Arrays.stream(UserRole.values()).filter(r -> r.getRoleType() == users.getRole()).collect(Collectors.toList());
		return new HotPepperBeautyUser(users, ur.get(0).getRoleName());
	}

}
