package com.hotpepper.beauty.demo;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import com.hotpepper.beauty.demo.model.HotPepperBeautyUser;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginUserHolder extends User {
	private static final long serialVersionUID = 1L;

	private final HotPepperBeautyUser hpbUser;

	public LoginUserHolder(HotPepperBeautyUser hpbUser) {
		// スーパークラスのユーザーID、パスワードに値をセットする
        // 実際の認証はスーパークラスのユーザーID、パスワードで行われる
		super(hpbUser.getUserId(), hpbUser.getPswd(), AuthorityUtils.createAuthorityList(hpbUser.getRole()));
		this.hpbUser = hpbUser;
	}
}
