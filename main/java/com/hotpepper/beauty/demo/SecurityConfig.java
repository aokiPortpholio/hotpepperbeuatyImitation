package com.hotpepper.beauty.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Override
	public void configure(WebSecurity websec) throws Exception {
		websec.ignoring().antMatchers("/webjar/**", "/css/**", "/pictures/**");
	}
	
	@Override
	public void configure(HttpSecurity httpsec) throws Exception {
		httpsec
			.authorizeRequests()
				.antMatchers("/webjars/**").permitAll()
				.antMatchers("/css/**").permitAll()
				.antMatchers("/").permitAll()
				.antMatchers("/**/index").permitAll()
				.antMatchers("/public/**").permitAll()
				.antMatchers("/html/**").permitAll()
				.anyRequest().authenticated();
		
		httpsec
			.formLogin()
				.loginProcessingUrl("/public/login")
				.loginPage("/public/login")
				.failureUrl("/error")
				.usernameParameter("userId")
				.passwordParameter("pswd")
				// 常にデフォルトログイン後ページに遷移しない（↓false)
				.defaultSuccessUrl("/public/index", false)
				.successHandler(new MyAuthenticationSuccessHandler())
				.and()
				// Cookie に JSESSIONID を保存。デフォルトでは30分ログイン不要になる。
				.rememberMe()
				// また、httpsでのみ送信設定にする。（通常のhttpではCookieが盗まれる可能性あり）
				.useSecureCookie(true);
		
		httpsec
			.logout()
				// GETメソッドでのログアウト後の遷移先
				.logoutRequestMatcher(new AntPathRequestMatcher("/public/logout"))
				// POSTメソッドでのログアウト後の遷移先
				.logoutUrl("/public/logout")
				.logoutSuccessUrl("/public/index"); 
        
		httpsec.csrf().disable();
		
	}
	
	@Configuration
	protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
		
		private UserDetailsService userDetailsService;
		
		@Autowired
		public AuthenticationConfiguration(UserDetailsService userDetailsService) {
			this.userDetailsService = userDetailsService;
		}
		
		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			auth
				.userDetailsService(userDetailsService)
				.passwordEncoder(new BCryptPasswordEncoder());
		}
	}
}
