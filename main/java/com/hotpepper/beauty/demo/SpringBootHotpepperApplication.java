package com.hotpepper.beauty.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHotpepperApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHotpepperApplication.class, args);
	}

}
