package com.hotpepper.beauty.demo.controller;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.hotpepper.beauty.demo.common.MyLogger;
import com.hotpepper.beauty.demo.model.MenuDetail;
import com.hotpepper.beauty.demo.model.enums.MenuItem;
import com.hotpepper.beauty.demo.model.form.MenuFilter;
import com.hotpepper.beauty.demo.service.MenuService;

@Controller
public class MenuController {
	private static final String TO_INDEX = "/public/index";
	
	private MenuFilter menufilter;
	private final MenuService service;
	
	@Autowired
	public MenuController(MenuFilter menufilter, MenuService service) {
		this.menufilter = menufilter;
		this.service = service;
	}
	
	@GetMapping({"/", "/**/index"})
	public String toIndex(Model model) {
		MyLogger.logging("   toIndex IN!!!");
		List<MenuDetail> menuDtls = service.getMenuListAll();
		menuDtls.forEach(f->MyLogger.logging(f.toString()));
		
		model.addAttribute("menuItems1", MENU_ITEMS_LINE1);
		model.addAttribute("menuItems2", MENU_ITEMS_LINE2);
		model.addAttribute("menuFilter", menufilter);
		model.addAttribute("menuDetails", menuDtls);
		
		return TO_INDEX;
	}
	
	@PostMapping("/public/fillterMenu")
	public String filterMenu(MenuFilter filter, Model model) {
		MyLogger.logging("   filter -> " + filter.toString());
		List<MenuDetail> menuDtls = service.getMenuList(filter);
		menuDtls.forEach(f->MyLogger.logging(f.toString()));
		
		model.addAttribute("menuItems1", MENU_ITEMS_LINE1);
		model.addAttribute("menuItems2", MENU_ITEMS_LINE2);
		model.addAttribute("menuFilter", filter);
		model.addAttribute("menuDetails", menuDtls);
		
		return TO_INDEX;
	}
	
	@SuppressWarnings("serial")
	final static Map<String, String> MENU_ITEMS_LINE1 =
			Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
				{
					put(MenuItem.CUT.getDisp(), MenuItem.CUT.getKind());
					put(MenuItem.COLOR.getDisp(), MenuItem.COLOR.getKind());
					put(MenuItem.PERM.getDisp(), MenuItem.PERM.getKind());
					put(MenuItem.STRAIGHT.getDisp(), MenuItem.STRAIGHT.getKind());
					put(MenuItem.EXTENTION.getDisp(), MenuItem.EXTENTION.getKind());
				}
			});
	
	@SuppressWarnings("serial")
	final static Map<String, String> MENU_ITEMS_LINE2 =
			Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
				{
					put(MenuItem.TREATMENT.getDisp(), MenuItem.TREATMENT.getKind());
					put(MenuItem.HAIRSET.getDisp(), MenuItem.HAIRSET.getKind());
					put(MenuItem.HEADSPA.getDisp(), MenuItem.HEADSPA.getKind());
					put(MenuItem.DRESSING.getDisp(), MenuItem.DRESSING.getKind());
					put(MenuItem.OTHER.getDisp(), MenuItem.OTHER.getKind());
				}
			});
}
