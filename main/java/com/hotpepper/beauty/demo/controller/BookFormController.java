package com.hotpepper.beauty.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BookFormController {
	
	private static final String TO_BOOK_FORM = "/authed/bookForm";

	@GetMapping("authed/toBookForm")
	public String toLoginGet() {
		return TO_BOOK_FORM;
	}
	@PostMapping("authed/toBookForm")
	public String toLoginPost() {
		return TO_BOOK_FORM;
	}
}
