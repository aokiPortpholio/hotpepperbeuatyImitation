package com.hotpepper.beauty.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {
	
	private static final String TO_LOGIN = "/public/login";

	@GetMapping(TO_LOGIN)
	public String toLoginGet() {
		return TO_LOGIN;
	}
	
	@PostMapping(TO_LOGIN)
	public String toLoginPost() {
		return TO_LOGIN;
	}
}
