package com.hotpepper.beauty.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hotpepper.beauty.demo.model.ImmutableActiveStylists;
import com.hotpepper.beauty.demo.model.ImmutableBookTable;
import com.hotpepper.beauty.demo.model.MenuDetail;
import com.hotpepper.beauty.demo.repository.dao.Stylist;
import com.hotpepper.beauty.demo.service.BookService;

@Controller
public class BookController {
	private static final String TO_BOOK_TABLE = "/public/booktable";
	private static final String TO_BOOK_TABLE_SELECTED_STYLIST = "/public/booktableSelectedStylist";
	private static final String TO_SELECT_STYLIST = "/public/selectStylist";
	private static final String TO_SELECT_ACTIVE_STYLIST = "/public/selectActiveStylist";

	private BookService bookService;
	
	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	
	@GetMapping("public/toBookTable")
	public String toBookTable(@RequestParam(name = "menuId", required = false) String menuId, Model model) {
		MenuDetail menuDetail = bookService.selectMenuDetail(menuId);
		ImmutableBookTable bookTable = bookService.getImmutableBookTableMap(null);
		
		model.addAttribute("menuDetail", menuDetail);
		model.addAttribute("stylist", null);
		model.addAttribute("bookTable", bookTable);
		return TO_BOOK_TABLE;
	}
	
	@PostMapping("public/toBooktableSelectedStylist")
	public String tobooktableSelectedSylist(@RequestParam(name = "menuId", required = false) String menuId,
			@RequestParam(name = "stylistId", required = false) String stylistId, Model model) {

		MenuDetail menuDetail = bookService.selectMenuDetail(menuId);
		Stylist stylist = bookService.selectStylist(stylistId);
		ImmutableBookTable bookTable = bookService.getImmutableBookTableMap(stylistId);
		
		model.addAttribute("menuDetail", menuDetail);
		model.addAttribute("stylist", stylist);
		model.addAttribute("bookTable", bookTable);
		return TO_BOOK_TABLE_SELECTED_STYLIST;
	}
	
	@GetMapping("public/toSelectStylist")
	public String toSelectStylist(@RequestParam(name = "menuId", required = false) String menuId, Model model) {
		MenuDetail menuDetail = bookService.selectMenuDetail(menuId);
		Map<String, List<Stylist>> stylistMap = bookService.getUnmodifiableStylistMap();
		
		model.addAttribute("menuDetail", menuDetail);
		model.addAttribute("stylistMap", stylistMap);
		return TO_SELECT_STYLIST;
	}
	
	
	@GetMapping("public/toSelectActiveStylist")
	public String toSelectActiveStylist(@RequestParam(name = "menuId", required = false) String menuId,
			@RequestParam(name = "date", required = false) String date,
			@RequestParam(name = "time", required = false) String time, Model model) {
		
		String visitDate = bookService.getVisitDate(date);
		ImmutableActiveStylists activeStylists = bookService.getUnmodifiableActiveStylistMap(date, time);
		
		model.addAttribute("menuId", menuId);
		model.addAttribute("date", date);
		model.addAttribute("time", time);
		
		model.addAttribute("visitDate", visitDate);
		model.addAttribute("activeStylistMap", activeStylists.getActiveStylistMap());
		model.addAttribute("nonActiveStylistMap", activeStylists.getNonActiveStylistMap());
		
		return TO_SELECT_ACTIVE_STYLIST;
	}
}
