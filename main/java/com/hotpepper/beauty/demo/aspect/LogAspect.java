package com.hotpepper.beauty.demo.aspect;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.hotpepper.beauty.demo.common.MyLogger;


@Aspect
@Component
public class LogAspect {

	@Before(value = "execution(* com.hotpepper.beauty.demo.controller.*Controller.*(..))")
	public void beforeLogging() {
		LocalDateTime now = LocalDateTime.now();
		MyLogger.logging("   !!コントローラー開始!! at: " + now.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")));
	}
	
	@After(value = "execution(* com.hotpepper.beauty.demo.controller.*Controller.*(..))")
	public void afterLogging() {
		LocalDateTime now = LocalDateTime.now();
		MyLogger.logging("   !!コントローラー終了!! at: " + now.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")));
	}
	
	@Before(value = "execution(* com.hotpepper.beauty.demo.controller.*Controller.*(..))")
	public void printArgs(JoinPoint jp) {
		Object[] objs = jp.getArgs();
		if (objs != null) Arrays.asList(objs).forEach(o->MyLogger.logging("   引数：" + o.toString()));
	}
}
