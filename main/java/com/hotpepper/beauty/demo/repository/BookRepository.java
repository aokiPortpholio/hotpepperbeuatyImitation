package com.hotpepper.beauty.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.hotpepper.beauty.demo.repository.dao.Book;
import com.hotpepper.beauty.demo.repository.dao.CutExamplePicture;
import com.hotpepper.beauty.demo.repository.dao.DayOff;
import com.hotpepper.beauty.demo.repository.dao.ShiftType;
import com.hotpepper.beauty.demo.repository.dao.Stylist;

@Repository
@Mapper
public interface BookRepository {
	
	public List<Stylist> selectStylistList(@Param("paramId") String paramId);
	public List<ShiftType> selectShiftType(@Param("inStatement") String inStatement);
	public List<DayOff> selectDayOff(@Param("date") int date, @Param("stylistId") String stylistId);
	public List<Book> selectBook(@Param("stylistId") String stylistId);
	public List<CutExamplePicture> selectCutExamplePictures(@Param("paramId") String paramId);
}
