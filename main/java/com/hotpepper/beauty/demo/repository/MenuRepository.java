package com.hotpepper.beauty.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.hotpepper.beauty.demo.repository.dao.MenuDetailSelect;

@Mapper
@Repository
public interface MenuRepository {
	public List<MenuDetailSelect> selectMenuDetailAll();
	public List<MenuDetailSelect> selectMenuDetailList(@Param("menuKinds") String menuKinds);
	public List<Integer> selectMenuKind(@Param("menuId") String menuId);
	public MenuDetailSelect selectMenuDetail(@Param("menuId") String menuId);
}
