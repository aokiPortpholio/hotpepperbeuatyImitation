package com.hotpepper.beauty.demo.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.hotpepper.beauty.demo.repository.dao.Users;

@Mapper
@Repository
public interface UserRepository {
	public Users selectUsers(@Param("loginId") String loginId);
}
