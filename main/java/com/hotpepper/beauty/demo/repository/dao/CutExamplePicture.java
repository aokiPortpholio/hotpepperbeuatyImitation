package com.hotpepper.beauty.demo.repository.dao;

import lombok.Data;

@Data
public class CutExamplePicture {
	private String stylistId;
	private int pictureId;
	private String url;
	private String comment;
}
