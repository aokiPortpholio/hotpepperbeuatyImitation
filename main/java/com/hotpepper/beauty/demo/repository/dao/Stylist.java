package com.hotpepper.beauty.demo.repository.dao;

import java.util.List;

import lombok.Data;

@Data
public class Stylist {
	private String stylistId;
	private String password;
	private int rank;
	private String name;
	private int shiftType;
	private String picture;
	private int reservationFee;
	private String comment;
	private String commentDetail;
	private int experiencedYears;
	private String specialty;
	private String specialeTechnique;
	private String recentInterested;
	private int role;
	
	private List<CutExamplePicture> cutExamples;
}
