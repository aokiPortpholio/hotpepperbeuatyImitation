package com.hotpepper.beauty.demo.repository.dao;

import lombok.Data;

@Data
public class Book {
	private int date;
	private int timezone;
	private String stylistId;
	private String guestId;
	private String request;
	private int price;
	private boolean cancel;
}
