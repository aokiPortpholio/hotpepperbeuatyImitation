package com.hotpepper.beauty.demo.repository.dao;

import java.io.Serializable;

import lombok.Data;

@Data
public class Users implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String userId;
	private String pswd;
	private int role;
}
