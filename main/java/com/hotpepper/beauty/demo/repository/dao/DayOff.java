package com.hotpepper.beauty.demo.repository.dao;

import lombok.Data;

@Data
public class DayOff {
	private int date;
	private int timezone;
	private String stylistId;
	private String reason;
}
