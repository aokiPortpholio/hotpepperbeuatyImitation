package com.hotpepper.beauty.demo.repository.dao;

import lombok.Data;

@Data
public class ShiftType {
	private int shiftType;
	private int weekKind;
	private int workInTimezone;
	private int workOutTimezone;	
}
