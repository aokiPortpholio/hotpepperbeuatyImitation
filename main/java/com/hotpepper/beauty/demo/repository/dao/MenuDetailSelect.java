package com.hotpepper.beauty.demo.repository.dao;

import lombok.Data;

@Data
public class MenuDetailSelect {
	private String id;
	private String name;
	private String explanation;
	private int price;
	private int requiredTime;
	private int couponScope;
	private int visitCondition;
	private String otherCondition;
	private String pictures;
}
