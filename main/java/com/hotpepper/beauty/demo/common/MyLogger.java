package com.hotpepper.beauty.demo.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.LoggingUtils;

public class MyLogger {
	private final static Logger logger = LoggerFactory.getLogger(LoggingUtils.class);
	public static enum LogLevel {
		INF,
		WRN,
		ERR,
		DBG;
	}
	public static void logging(String msg) {
		logger.info(msg);
	}
	public static void logging(Object msg) {
		logger.info(msg.toString());
	}
	public static void logging(LogLevel level, String msg) {
		if (level == LogLevel.INF) logger.info(msg);
		if (level == LogLevel.WRN) logger.warn(msg);
		if (level == LogLevel.ERR) logger.error(msg);
		if (level == LogLevel.DBG) logger.debug(msg);
	}
}
