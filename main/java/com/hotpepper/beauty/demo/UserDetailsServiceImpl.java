package com.hotpepper.beauty.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.hotpepper.beauty.demo.model.HotPepperBeautyUser;
import com.hotpepper.beauty.demo.service.UserService;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	private UserService service;
	
	@Autowired
	public UserDetailsServiceImpl(UserService service) {
		this.service = service;
	}
	
	@Override
	public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
		HotPepperBeautyUser hpbUser = null;
		
        try {
            // 入力したユーザーIDから認証を行うユーザー情報を取得する
        	hpbUser = HotPepperBeautyUser.getInstance(service.getUsers(loginId));
        } catch (Exception e) {
            throw new UsernameNotFoundException("It can not be acquired User");
        }
        
        if(hpbUser == null){
            throw new UsernameNotFoundException("User not found for login id: " + loginId);
        }
        
        // ユーザー情報が取得できたらSpring Securityで認証できる形で戻す
        return new LoginUserHolder(hpbUser);
	}

}
