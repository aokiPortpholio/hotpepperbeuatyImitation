insert into TBL_MENU_LIST (menu_id, menu_kind) values (10001,0);
insert into TBL_MENU_LIST (menu_id, menu_kind) values (10001,1);
insert into TBL_MENU_LIST (menu_id, menu_kind) values (10001,2);
insert into TBL_MENU_LIST (menu_id, menu_kind) values (10002,0);
insert into TBL_MENU_LIST (menu_id, menu_kind) values (10002,2);
insert into TBL_MENU_LIST (menu_id, menu_kind) values (10003,0);


insert into TBL_MENU_DETAILS (menu_id, menu_name, detail, price, required_time, coupon_scope, visit_condition, other_condition, pictures)
values (
10001,
'いい感じカット＆カラー＆パーマ',
'いい感じにカット＆カラー＆パーマします。',
6000,
90,
0,
0,
'特になし',
'/pictures/cat1.jpg'
);
insert into TBL_MENU_DETAILS (menu_id, menu_name, detail, price, required_time, coupon_scope, visit_condition, other_condition, pictures)
values (
10002,
'いい感じカット＆パーマ',
'いい感じにカット＆パーマします。',
9000,
120,
1,
1,
'その他クーポンの併用不可 指名不可',
'/pictures/cat1.jpg'
);
insert into TBL_MENU_DETAILS (menu_id, menu_name, detail, price, required_time, coupon_scope, visit_condition, other_condition, pictures)
values (
10003,
'いい感じカット',
'いい感じにカットします。',
3000,
60,
1,
0,
'その他クーポンの併用不可',
'/pictures/cat1.jpg'
);

insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	0,	'stylist1@gmail.com',	'guest1@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	1,	'stylist1@gmail.com',	'guest2@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	1,	'stylist2@gmail.com',	'guest3@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	2,	'stylist1@gmail.com',	'guest4@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	2,	'stylist2@gmail.com',	'guest5@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	2,	'stylist3@gmail.com',	'guest6@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	3,	'stylist1@gmail.com',	'guest7@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	3,	'stylist2@gmail.com',	'guest8@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	3,	'stylist3@gmail.com',	'guest9@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	3,	'stylist4@gmail.com',	'guest10@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	4,	'stylist1@gmail.com',	'guest11@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	4,	'stylist2@gmail.com',	'guest12@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	4,	'stylist3@gmail.com',	'guest13@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	4,	'stylist4@gmail.com',	'guest14@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	4,	'stylist5@gmail.com',	'guest15@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	5,	'stylist1@gmail.com',	'guest16@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	5,	'stylist2@gmail.com',	'guest17@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	5,	'stylist3@gmail.com',	'guest18@gmail.com',	'いい感じにオナシャス',	3000,	TRUE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	5,	'stylist4@gmail.com',	'guest19@gmail.com',	'いい感じにオナシャス',	3000,	TRUE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	5,	'stylist5@gmail.com',	'guest20@gmail.com',	'いい感じにオナシャス',	3000,	TRUE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	6,	'stylist1@gmail.com',	'guest21@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	6,	'stylist2@gmail.com',	'guest22@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);
insert into 	tbl_book 	(	date,	timezone,	stylist_id,	guest_id,	request,	price,	cancel	) values (	20210413,	6,	'stylist3@gmail.com',	'guest23@gmail.com',	'いい感じにオナシャス',	3000,	FALSE	);

insert into  tbl_stylist ( id, password, rank, name, shift_type, picture, reservation_fee, comment, comment_detail, experienced_years, specialty, speciale_technique, recent_interested, role ) values ( 'stylist1@gmail.com', 'P@SSWORD', 3, '伝説のカット1', 3, '/pictures/cat1.jpg', 0, '伝説です', '最近は名前も聞かない', 3, 'カリスマカット', 'カリスマカッティング', 'ゲートボール', 0 );
insert into  tbl_stylist ( id, password, rank, name, shift_type, picture, reservation_fee, comment, comment_detail, experienced_years, specialty, speciale_technique, recent_interested, role ) values ( 'stylist2@gmail.com', 'P@SSWORD', 3, '伝説のカット2', 1, '/pictures/cat1.jpg', 500, '伝説です', '最近は名前も聞かない', 8, 'カリスマカット', 'カリスマカッティング', 'ゲートボール', 0 );
insert into  tbl_stylist ( id, password, rank, name, shift_type, picture, reservation_fee, comment, comment_detail, experienced_years, specialty, speciale_technique, recent_interested, role ) values ( 'stylist3@gmail.com', 'P@SSWORD', 2, '伝説のカット3', 1, '/pictures/cat1.jpg', 500, '伝説です', '最近は名前も聞かない', 10, 'カリスマカット', 'カリスマカッティング', 'ゲートボール', 0 );
insert into  tbl_stylist ( id, password, rank, name, shift_type, picture, reservation_fee, comment, comment_detail, experienced_years, specialty, speciale_technique, recent_interested, role ) values ( 'stylist4@gmail.com', 'P@SSWORD', 2, '伝説のカット4', 2, '/pictures/cat1.jpg', 1000, '伝説です', '最近は名前も聞かない', 12, 'カリスマカット', 'カリスマカッティング', 'ゲートボール', 1 );
insert into  tbl_stylist ( id, password, rank, name, shift_type, picture, reservation_fee, comment, comment_detail, experienced_years, specialty, speciale_technique, recent_interested, role ) values ( 'stylist5@gmail.com', 'P@SSWORD', 1, '伝説のカット5', 4, '/pictures/cat1.jpg', 1500, '伝説です', '最近は名前も聞かない', 15, 'カリスマカット', 'カリスマカッティング', 'ゲートボール', 1 );
insert into  tbl_stylist ( id, password, rank, name, shift_type, picture, reservation_fee, comment, comment_detail, experienced_years, specialty, speciale_technique, recent_interested, role ) values ( 'stylist6@gmail.com', 'P@SSWORD', 3, '伝説のカット6', 1, '/pictures/cat1.jpg', 500, '伝説です', '最近は名前も聞かない', 3, 'カリスマカット', 'カリスマカッティング', 'ゲートボール', 0 );

insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	1,	2,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	1,	3,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	1,	4,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	1,	5,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	1,	6,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	2,	3,	6,	21	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	2,	4,	6,	21	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	2,	5,	6,	21	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	2,	6,	6,	21	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	2,	7,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	3,	6,	6,	21	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	3,	7,	6,	21	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	4,	2,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	4,	3,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	4,	4,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	4,	5,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	4,	6,	0,	18	);
insert into 	tbl_shift_type 	(	shift_type,	week_kind,	workin_timezone,	workout_timezone	) values (	4,	7,	0,	18	);

insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210406,	0,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	1,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210407,	2,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	3,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	4,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	5,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	6,	'stylist4@gmail.com',	'私用'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	7,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	8,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	9,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	10,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	11,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	12,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210404,	13,	'stylist5@gmail.com',	'春季休暇'	);
insert into 	tbl_dayoff	(	date,	timezone,	stylist_id,	reason	) values (	20210408,	14,	'stylist5@gmail.com',	'春季休暇'	);

insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist1@gmail.com', 1, '/pictures/cat2.jpg', '丸みショートで長持ちショートボブ！次回来店までオシャレヘア。だがネコだ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist1@gmail.com', 2, '/pictures/cat3.jpg', 'コンパクトショートもドライのみでお手入れ簡単！オススメです。ネコを信じろ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist1@gmail.com', 3, '/pictures/cat4.jpg', '極細ハイライトで【脱白髪染め】もオススメ☆。ネコと和解せよ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist1@gmail.com', 4, '/pictures/cat5.jpg', '自然なグラデーションで【長持ちカラー】。ネコネコネコネコネコ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist2@gmail.com', 1, '/pictures/cat2.jpg', '丸みショートで長持ちショートボブ！次回来店までオシャレヘア。だがネコだ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist2@gmail.com', 2, '/pictures/cat3.jpg', 'コンパクトショートもドライのみでお手入れ簡単！オススメです。ネコを信じろ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist2@gmail.com', 3, '/pictures/cat4.jpg', '極細ハイライトで【脱白髪染め】もオススメ☆。ネコと和解せよ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist3@gmail.com', 1, '/pictures/cat2.jpg', '丸みショートで長持ちショートボブ！次回来店までオシャレヘア。だがネコだ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist3@gmail.com', 2, '/pictures/cat3.jpg', 'コンパクトショートもドライのみでお手入れ簡単！オススメです。ネコを信じろ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist4@gmail.com', 1, '/pictures/cat2.jpg', '丸みショートで長持ちショートボブ！次回来店までオシャレヘア。だがネコだ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist6@gmail.com', 1, '/pictures/cat2.jpg', '丸みショートで長持ちショートボブ！次回来店までオシャレヘア。だがネコだ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist6@gmail.com', 2, '/pictures/cat3.jpg', 'コンパクトショートもドライのみでお手入れ簡単！オススメです。ネコを信じろ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist6@gmail.com', 3, '/pictures/cat4.jpg', '極細ハイライトで【脱白髪染め】もオススメ☆。ネコと和解せよ。' );
insert into  tbl_cut_example_pictures ( stylist_id, picture_id, url, comment ) values ( 'stylist6@gmail.com', 4, '/pictures/cat5.jpg', '自然なグラデーションで【長持ちカラー】。ネコネコネコネコネコ。' );

insert into  tbl_users ( user_id, password, role ) values ( 'user1@gmail.com', '$2a$10$FEHO2IO4kmkvDSzpI87t/ehWqu7WUFOklLlbLnA5LZhyIAWeyyfT.', 0 );
insert into  tbl_users ( user_id, password, role ) values ( 'admin@gmail.com', '$2a$10$FEHO2IO4kmkvDSzpI87t/ehWqu7WUFOklLlbLnA5LZhyIAWeyyfT.', 1 );
