CREATE TABLE IF NOT EXISTS tbl_menu_list (
  menu_id INT, 
  menu_kind INT, 
  primary key(menu_id, menu_kind)
);

CREATE TABLE IF NOT EXISTS tbl_menu_kind (
  menu_kind INT primary key,
  menu_kind_code INT,
  menu_kind_name VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS tbl_menu_details (
  menu_id INT primary key,
  menu_name VARCHAR(200),
  detail VARCHAR(500),
  price INT,
  required_time INT,
  coupon_scope INT,
  visit_condition VARCHAR(50),
  other_condition VARCHAR(250),
  pictures VARCHAR(256)
);

CREATE TABLE IF NOT EXISTS tbl_book (
  date INT,
  timezone INT,
  stylist_id VARCHAR(100),
  guest_id VARCHAR(100),
  request VARCHAR(500),
  price INT,
  cancel BOOLEAN,
  primary key(date, timezone, stylist_id)
);

CREATE TABLE IF NOT EXISTS tbl_stylist (
  id VARCHAR(100) primary key,
  password VARCHAR(50),
  rank INT,
  name VARCHAR(50),
  shift_type INT,
  picture VARCHAR(256),
  reservation_fee INT,
  comment VARCHAR(100),
  comment_detail VARCHAR(500),
  experienced_years INT,
  specialty VARCHAR(50),
  speciale_technique VARCHAR(50),
  recent_interested VARCHAR(100),
  role INT
);

CREATE TABLE IF NOT EXISTS tbl_shift_type (
  shift_type INT,
  week_kind INT,
  workin_timezone INT,
  workout_timezone INT
);

CREATE TABLE IF NOT EXISTS tbl_dayoff (
  date INT,
  timezone INT,
  stylist_id VARCHAR(100),
  reason VARCHAR(50),
  primary key(date, timezone, stylist_id)
);

CREATE TABLE IF NOT EXISTS tbl_cut_example_pictures (
  stylist_id VARCHAR(100),
  picture_id INT,
  url VARCHAR(256),
  comment VARCHAR(500),
  primary key(stylist_id, picture_id)
);

CREATE TABLE IF NOT EXISTS tbl_users (
  user_id VARCHAR(100),
  password VARCHAR(1000),
  role INT,
  primary key(user_id)
);